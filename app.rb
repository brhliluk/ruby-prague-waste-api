# frozen_string_literal: true

require 'sinatra'
require 'sinatra/cross_origin'
require 'sinatra/activerecord'
require 'sinatra/param'
require 'kaminari/activerecord'
require 'kaminari/sinatra'
require './config/cors'
require './models/bin'
require 'json'
require 'pg_search'

register Kaminari::Helpers::SinatraHelpers
helpers Sinatra::Param

get '/bins' do
  content_type :json

  param :lat, Float, min: -90, max: 90, required: true
  param :long, Float, min: -180, max: 180, required: true
  param :radius, Float, default: 5
  param :filter, Array, default: [*0..5]
  param :allRequired, Sinatra::Param::Boolean, default: false
  param :page, Integer, default: 1
  param :perPage, Integer, default: 25

  operator = params[:allRequired] ? '@>' : '&&'
  Bin.where(['trash_types %s ARRAY%s', operator, params[:filter].map(&:to_i)])
     .near([params[:lat], params[:long]], params[:radius], units: :km)
     .page(params[:page])
     .per(params[:perPage])
     .to_json
end

get '/download' do
  file_path = './public/db.json'
  unless File.file?(file_path)
    File.open(file_path, 'w') do |f|
      f.write(Bin.all.to_json)
    end
  end
  etag File.read(file_path).hash
  send_file file_path, filename: 'db.json', type: 'Application/octet-stream'
end

get '/bins-search' do
  content_type :json

  param :searchQuery, String, required: true
  param :filter, Array, default: [*0..5]
  param :allRequired, Sinatra::Param::Boolean, default: false
  param :page, Integer, default: 1
  param :perPage, Integer, default: 25

  operator = params[:allRequired] ? '@>' : '&&'
  Bin.where(['trash_types %s ARRAY%s', operator, params[:filter].map(&:to_i)])
     .search_by_address(params[:searchQuery])
     .page(params[:page])
     .per(params[:perPage])
     .to_json
end

get '/bins-all' do
  content_type :json
  Bin.all.to_json
end

after do
  # Close the connection after the request is done so that we don't
  # deplete the ActiveRecord connection pool.
  ActiveRecord::Base.connection.close
end
