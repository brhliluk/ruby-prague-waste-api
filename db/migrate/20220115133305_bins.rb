# frozen_string_literal: true

class Bins < ActiveRecord::Migration[7.0]
  def up
    create_table :bins do |b|
      b.float :latitude
      b.float :longitude
      b.integer :trash_types, array: true, default: []
      b.text :address
    end
    execute 'CREATE EXTENSION pg_trgm;'
  end

  def down
    drop_table :bins
    execute 'DROP EXTENSION pg_trgm;'
  end
end
