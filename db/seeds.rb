# frozen_string_literal: true

require 'json'
require 'geocoder'
require 'progress_bar'
require './models/bin'

Bin.delete_all

puts 'Creating bins'
# Use demo env variable to select either small portion of db or full (full by default)
file = if ENV['demo']
         File.read('./db/OpenDataBins.json')
       else
         File.read('./db/OpenDataBinsFull.json')
       end
data = JSON.parse(file)

def get_trash_type(czech_name)
  case czech_name
  when 'Papír' then 0
  when 'Plast' then 1
  when 'Nápojové kartóny' then 2
  when 'Barevné sklo' then 3
  when 'Čiré sklo' then 4
  when 'Kovy' then 5
  else 6
  end
end

def build_address(address, parts)
  output = ""
  parts.each do |part|
    if address.key?(part)
      output += address[part]
      if part != parts.last
        output += ", "
      end
    end
  end
  output
end

def add_bin(lat, long, trash_type, retry_arr)
  entry = Bin.find_by(latitude: lat, longitude: long)
  if entry
    entry.trash_types << trash_type
    entry.save
  else
    begin
      addr = Geocoder.search([lat, long]).first.data["address"]
      address = build_address(addr, %w[amenity house_number road suburb])
      Bin.create(latitude: lat, longitude: long, trash_types: [trash_type],
                 address: address)
    # Catch API timeouts
    rescue NoMethodError
      retry_arr.push(Array[lat, long, trash_type])
    end
  end
end

# If geocoder API gets too many request and can not catch up with requests, increase the timeout (number is in seconds)
Geocoder.configure(
  timeout: 10,
  language: :cs,
  http_headers: { "User-Agent" => "Prague Waste - Masters thesis/Lukáš Brhlík - FIT CTU" }
)
to_retry = []
progressbar = ProgressBar.new(data['features'].length)
data['features'].each do |bin|

  long = bin['geometry']['coordinates'][0]
  lat = bin['geometry']['coordinates'][1]
  trash_type = get_trash_type(bin['properties']['TRASHTYPENAME'])

  add_bin(lat, long, trash_type, to_retry)
  progressbar.increment!
end

retry_bar = ProgressBar.new(to_retry.length)
until to_retry.empty?
  failed_bin = to_retry.pop
  add_bin(failed_bin[0], failed_bin[1], failed_bin[2], to_retry)
  retry_bar.increment!
end


