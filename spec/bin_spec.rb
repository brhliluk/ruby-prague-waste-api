require 'test/unit'
require 'rack/test'
require 'rack/test/body/json'
require 'rspec'
require 'rspec/expectations'
require 'sinatra'
require './app'

RSpec.describe 'Prague waste API, ' do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  it "search returns json" do
    get '/bins-search', { searchQuery: "chabr" }
    expect(last_response).to be_ok
    expect(last_response).to be_json
  end

  it "download returns json" do
    get '/download'
    expect(last_response).to be_ok
    expect(last_response.header['Content-Type']).to include 'Application/octet-stream'
  end

  it "list returns json" do
    get '/bins', { lat: 50.0809478, long: 14.4293336, filter: [1, 0], allRequired: true, radius: 20, page: 1, perPage: 25 }
    expect(last_response).to be_ok
    expect(last_response).to be_json
  end

  it "search has required params" do
    get '/bins-search'
    expect(last_response).not_to be_ok
  end

  it "list has required params" do
    get '/bins', { lat: 50.0809478, filter: [1, 0], allRequired: true, radius: 20, page: 1, perPage: 25 }
    expect(last_response).not_to be_ok
  end

  it "list has default params" do
    get '/bins', { long: 14.4293336, lat: 50.0809478 }
    expect(last_response).to be_ok
  end
end