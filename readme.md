# Prague-waste API

Simple API written on top of Sinatra framework. Displays bin locations from [opendata file](https://opendata.praha.eu/dataset/stanoviste-trideneho-odpadu-polozky) provided by Prague.
API documentation can be found [here](https://app.swaggerhub.com/apis/brhliluk/prague-waste/1.0.0-oas3?loggedInWithGitHub=true).

## Installation

Clone, install dependencies, setup database (`config/environments.rb`), run migrations, add initial data, start server

* `git clone git@gitlab.fit.cvut.cz:brhliluk/ruby-prague-waste-api.git`
* `bundle install`
* `rake db:migrate`
* `rake db:seed demo=true` for demo (full db is huge, without `demo=true` for full db)
* `bundle exec rackup` or `shotgun config.ru`

## Deployment

This app is already deployed on Heroku.

[Prague Waste API on Heroku](https://prague-waste-api.herokuapp.com/)

## Usage

Example endpoints to make it easier for you.

`http://127.0.0.1:9292/bins?lat=50.0809478&long=14.4293336&filter=[0,1]&allRequired=false&radius=20&page=1&perPage=25`

`http://127.0.0.1:9292/bins-search?searchQuery="chabr"`

`http://127.0.0.1:9292/download` (8.5mb file)