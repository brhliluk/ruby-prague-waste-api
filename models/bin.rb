# frozen_string_literal: true

require 'geocoder'
require 'pg_search'

class Bin < ActiveRecord::Base
  extend Geocoder::Model::ActiveRecord
  include PgSearch::Model
  # Used for searching by nearest locations
  reverse_geocoded_by :latitude, :longitude
  # Used for fuzzy search
  pg_search_scope :search_by_address, against: :address, using: { trigram: { word_similarity: true } }

  validates :latitude, numericality: { greater_than_or_equal_to: -90, less_than_or_equal_to: 90 }
  validates :longitude, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180 }
  validates :trash_types, presence: true
  validates :address, presence: true
end
